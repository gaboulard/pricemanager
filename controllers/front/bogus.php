<?php
if (!defined('_PS_VERSION_'))
	exit;

class pricemanagerBogusModuleFrontController extends ModuleFrontController
{
	public function init()
	{
		$this->display_column_left = false;
		$this->display_header = false;
		$this->display_footer = false;
		$this->display_column_right = false;
		parent::init();
	}

	public function initContent()
	{
		echo 'This is not \'Nam, this is bowling. There are rules.';
	}
}