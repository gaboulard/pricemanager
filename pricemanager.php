<?php
if (!defined('_PS_VERSION_'))
	exit;

class PriceManager extends Module
{
	public function __construct()
	{
		$this->name = 'pricemanager';
		$this->tab = 'administration';
		$this->version = '0.1';
		$this->author = 'Gaetan Boulard';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Price Manager');
		$this->description = $this->l('Modify prices of all articles and carts in 1 click');

		if (Configuration::get('PRICEMANAGER_CHECK') === false)
			$this->warning = $this->l('No check provided. ');
		if (Configuration::get('PRICEMANAGER_VALUE') === false)
			$this->warning .= $this->l('No value provided. ');
		if (Configuration::get('PRICEMANAGER_TYPE') === false)
			$this->warning .= $this->l('No type provided. ');
	}

	public function install()
	{
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

		if (!parent::install() || !$this->registerHook('backOfficeHeader')
			|| !Configuration::updateValue('PRICEMANAGER_CHECK', '0')
			|| !Configuration::updateValue('PRICEMANAGER_VALUE', '0')
			|| !Configuration::updateValue('PRICEMANAGER_TYPE', '0'))
			return false;
		$this->addFriendlyUrl();
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall()
			|| !Configuration::deleteByName('PRICEMANAGER_CHECK')
			|| !Configuration::deleteByName('PRICEMANAGER_VALUE')
			|| !Configuration::deleteByName('PRICEMANAGER_TYPE'))
			return false;
		$this->delFriendlyUrl();
		return true;
	}

	public function hookBackOfficeHeader()
	{
		$this->context->controller->addjQuery();
		$this->context->controller->addJS($this->_path.'pricemanager.js');
 	}

	private function postProcess()
	{
		$output = null;

		if (Tools::isSubmit('submit'.$this->name))
		{
			Configuration::updateValue('PRICEMANAGER_CHECK', Tools::getValue('check_0'));
			if (Tools::getValue('check_0'))
				return $this->displayError('Prices are locked');
			Configuration::updateValue('PRICEMANAGER_CHECK', Tools::getValue('check_0'));
			Configuration::updateValue('PRICEMANAGER_VALUE', (int)Tools::getValue('value'));
			Configuration::updateValue('PRICEMANAGER_TYPE', Tools::getValue('type'));
			if (Tools::getValue('target') == 0)
				$output .= $this->updateProductPriceAll();
			else
				$output .= $this->updateOrdersPriceAll();
			$output .= $this->displayConfirmation($this->l('Prices updated'));
		}
		return $output;
	}

	private function displayForm()
	{
		$options1 = array(
			array(
				'id_option' => '0',
				'name' => $this->l('Products')
			),
			array(
				'id_option' => '1',
				'name' => $this->l('Orders')
			)
		);
		$options2 = array(
			array(
				'id_option' => '0',
				'name' => $this->l('ON')
			)
		);
		$options3 = array(
			array(
				'id_option' => '0',
				'name' => $this->l('Value')
			),
			array(
				'id_option' => '1',
				'name' => $this->l('Percentage')
			)
		);
		$disabled = Configuration::get('PRICEMANAGER_CHECK');
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
				'icon' => 'icon-cog'
			),
			'input' => array(
				array(
					'type' => 'select',
					'label' => 'Modify:',
					'desc' => 'Choose the target of the modification',
					'name' => 'target',
					'options' => array(
						'query' => $options1,
						'id' => 'id_option',
						'name' => 'name'
					)
				),
				array(
					'type' => 'checkbox',
					'label' => 'Lock Prices',
					'name' => 'check',
					'values' => array(
						'query' => $options2,
						'id' => 'id_option',
						'name' => 'name'
					)
				),
				array(
					'type' => 'text',
					'label' => $this->l('Price Modification'),
					'name' => 'value',
					'disabled' => $disabled
				),
				array(
					'type' => 'select',
					'label' => 'Modification type',
					'name' => 'type',
					'options' => array(
						'query' => $options3,
						'id' => 'id_option',
						'name' => 'name'
					),
					'disabled' => $disabled
				)
			),
			'submit' => array(
				'title' => $this->l('Update Prices')
			)
		);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$helper->allow_employee_form_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$helper->submit_action = 'submit'.$this->name;
		$helper->fields_value = array(
			'target' => 0,
			'check_0' => $disabled,
			'value' => Configuration::get('PRICEMANAGER_VALUE'),
			'type' => Configuration::get('PRICEMANAGER_TYPE')
		);
		return $helper->generateForm($fields_form);
	}

	public function getContent()
	{
		return $this->postProcess().$this->displayForm();
	}

	private function addFriendlyUrl()
	{
		Db::getInstance()->insert('meta', array(
			'page' => 'module-pricemanager-bogus',
			'configurable' => 1
		));
		$id_meta = Db::getInstance()->Insert_ID();
		
		$query = 'SELECT id_shop FROM '._DB_PREFIX_.'shop;';
		$rows1 = Db::getInstance()->executeS($query);
		foreach ($rows1 as $key => $id_shop)
		{
			$query = 'SELECT id_lang FROM '._DB_PREFIX_.'lang;';
			$rows2 = Db::getInstance()->executeS($query);
			foreach ($rows2 as $id_lang)
			{
				Db::getInstance()->insert('meta_lang', array(
					'id_meta' => $id_meta,
					'id_shop' => $id_shop['id_shop'],
					'id_lang' => $id_lang['id_lang'],
					'title' => 'Bogus',
					'description' => '',
					'keywords' => '',
					'url_rewrite' => 'bogus'
				));
			}
		}
	}

	private function delFriendlyUrl()
	{
		$query = 'SELECT id_meta FROM '._DB_PREFIX_.'meta WHERE page = \'module-pricemanager-bogus\';';
		$result = Db::getInstance()->getValue($query);
		Db::getInstance()->delete('meta', 'page = \'module-pricemanager-bogus\'');
		Db::getInstance()->delete('meta_lang', 'id_meta = '.$result);
	}

	private function updateProductPriceAll()
	{
		$value = Configuration::get('PRICEMANAGER_VALUE');
		$percent = 1 + $value / 100;
		$type = Configuration::get('PRICEMANAGER_TYPE');

		if ($type == 0)
			$query = 'UPDATE '._DB_PREFIX_.'product SET price = price + '.$value.';';
		else
			$query = 'UPDATE '._DB_PREFIX_.'product SET price = price * '.$percent.';';
		$query .= 'UPDATE '._DB_PREFIX_.'product_shop AS ps
			JOIN '._DB_PREFIX_.'product AS p ON ps.id_product = p.id_product
			SET ps.price = p.price;';

		Db::getInstance()->execute($query);
	}

	private function updateOrdersPriceAll()
	{
		$value = Configuration::get('PRICEMANAGER_VALUE');
		$percent = 1 + $value / 100;
		$type = Configuration::get('PRICEMANAGER_TYPE');

		if ($type == 0)
		{
			$query = 'UPDATE '._DB_PREFIX_.'orders SET total_paid_tax_excl = total_paid_tax_excl + '.$value.';';
			$query .= 'UPDATE '._DB_PREFIX_.'orders SET total_paid_tax_incl = total_paid_tax_incl + '.$value.';';
			$query .= 'UPDATE '._DB_PREFIX_.'orders SET total_paid = total_paid + '.$value.';';
		}
		else
		{
			$query = 'UPDATE '._DB_PREFIX_.'orders SET total_paid_tax_excl = total_paid_tax_excl * '.$percent.';';
			$query .= 'UPDATE '._DB_PREFIX_.'orders SET total_paid_tax_incl = total_paid_tax_incl * '.$percent.';';
			$query .= 'UPDATE '._DB_PREFIX_.'orders SET total_paid = total_paid * '.$percent.';';
		}

		Db::getInstance()->execute($query);
	}
}
